[![Build Status](https://jenkins.spaghet.us/buildStatus/icon?job=beanify)](https://jenkins.spaghet.us/job/beanify/)

# beanify
WebExtension that replaces images with beans

## Usage
* Install dependencies with `yarn`
* Run `build.sh`
* Install the resulting .zip in your browser of choice (Chrome, Firefox, Edge, and derivatives should work.)
* Uninstall it, because it will kill your browser's performance.

## Builds

The latest build can be downloaded [here](https://jenkins.spaghet.us/job/beanify/lastSuccessfulBuild/artifact/dist/beanify.zip)
