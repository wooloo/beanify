const beansCount = 4;

beanify()
window.setInterval(beanify, 1000)

function rand (max) {
	return Math.floor(Math.random() * max);
};

async function beanify () {
	const elements = document.all;
	affectedCounter = 0;
	console.log(`Beanifying ${elements.length} elements...`);
	for (var i = 0; i < elements.length; i++) {
		const url = browser.runtime.getURL(`res/${rand(beansCount)}.jpg`);
		var affected = false;
		const el = elements[i];
		try {
			if (el.tagName == 'IMG') {
				if (!el.src.match(/res\/\d\.jpg/)) {
					el.src = url;
					affected = true;
				}
			}
			const style = getComputedStyle(el);
			if (style.backgroundImage != 'none') {
				if (!style.backgroundImage.match(/url\(.*res\/\d\.jpg\)/)) {
					affected = true;
					style.backgroundImage = `url(${url})`;
				}
			}
		} catch (e) {
			affected = false;
			console.error(e);
		}
		if (affected) {
			affectedCounter++;
			console.log(el);
		}
	}
	console.log(`${affectedCounter} of ${elements.length} elements affected.`);
}